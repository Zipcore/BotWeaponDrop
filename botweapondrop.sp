#pragma semicolon 1

#define PLUGIN_VERSION "1.0"

#include <sourcemod>
#include <sdktools>
#include <cstrike>
#include <smlib>

public Plugin myinfo = 
{
	name = "Bot Weapon Drop",
	author = ".#Zipcore",
	description = "",
	version = PLUGIN_VERSION,
	url = "zipcore.net"
};

#define LoopClients(%1) for(int %1 = 1; %1 <= MaxClients; %1++)

float g_fUnlock[MAXPLAYERS + 1];

public void OnMapStart()
{
	LoopClients(i)
		g_fUnlock[i] = 0.0;
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon, int &subtype, int &cmdnum, int &tickcount, int &seed, int mouse[2])
{
	if(!IsPlayerAlive(client) || IsFakeClient(client))
		return Plugin_Continue;
	
	if(!(buttons & IN_USE))
		return Plugin_Continue;
		
	int target = GetClientAimTarget(client);
	
	if(!target || !IsFakeClient(target))
		return Plugin_Continue;
	
	if(GetClientTeam(client) != GetClientTeam(target))
		return Plugin_Continue;
	
	if(g_fUnlock[client] > GetGameTime())
		return Plugin_Continue;
	
	if(!Entity_InRange(client, target, 92.0))
		return Plugin_Continue;
	
	int iWeapon = Client_GetActiveWeapon(target);
	
	if(!iWeapon)
		return Plugin_Continue;
	
	CS_DropWeapon(target, iWeapon, true);
	g_fUnlock[client] = GetGameTime()+1.0;
	
	return Plugin_Continue;
}